import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-objetivo',
  templateUrl: './objetivo.component.html',
  styleUrls: ['./objetivo.component.scss']
})
export class ObjetivoComponent implements OnInit {
  public iconBat = '../../../assets/icons/bat_white.png';
  public iconBee = '../../../assets/icons/bee_white.png';

  constructor() { }

  ngOnInit(): void {
  }

}
