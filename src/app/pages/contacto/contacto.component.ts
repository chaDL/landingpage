/* Importaciones de Angular */
import { Component, OnInit } from '@angular/core';

/* Importaciones de formulario */
import { Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss']
})
export class ContactoComponent implements OnInit {
  /* Image Icon */
  public iconFb = '../../../assets/icons/facebook_white.png';
  public iconIG = '../../../assets/icons/instagram_white.png';
  public iconYT = '../../../assets/icons/youtube_white.png';

  /* Form */
  public contactForm: FormGroup;
  public name: AbstractControl;
  public lastName: AbstractControl;
  public phone: AbstractControl;
  public email: AbstractControl;
  public message: AbstractControl;

  /* Message */
  public errorMessageName: string;
  public errorMessageLastName: string;
  public errorMessagePhone: string;
  public errorMessageEmail: string;
  public errorMessage: string;

  constructor( private formBuilder: FormBuilder ) {

    // Inicializamos el controlador de nuestro formulario.
    this.contactForm = this.formBuilder.group({
      nameGroup     : ['', Validators.compose( [ Validators.required, Validators.pattern('[a-z A-Z]{3,20}') ] ) ],
      lastNameGroup : ['', Validators.compose( [ Validators.required, Validators.pattern('[a-z A-Z]{3,20}') ] ) ],
      phoneGroup    : ['', Validators.compose( [ Validators.required, Validators.pattern('[0-9]{10}') ] ) ],
      emailGroup    : ['', Validators.compose( [ Validators.required, Validators.email ] ) ],
      messageGroup  : ['', Validators.compose( [ Validators.required, Validators.pattern('[a-z A-Z]{3,100}') ] ) ]
    });

    // Asignación de los controles de nuestro grupo (contactForm) a nuestras variables asignadas
    // a nuestros inputs
    this.name     = this.contactForm.controls.nameGroup;
    this.lastName = this.contactForm.controls.lastNameGroup;
    this.phone    = this.contactForm.controls.phoneGroup;
    this.email    = this.contactForm.controls.emailGroup;
    this.message  = this.contactForm.controls.messageGroup;

  }

  ngOnInit(): void {
  }

  
  /**
   * @description: Método encargado de validar que el texto ingresado corresponda a las validaciones
   *               establecidas en el controlador de grupo (contactForm). En caso de ser inválido, 
   *               asignamos un mensaje de error.
   */
  public checkName() {

    if ( this.name.status === 'INVALID' ) {
      if ( this.name.value === '' ) {
        this.errorMessageName = 'Campo requerido';
      } else {
        this.errorMessageName = 'Valor incorrecto';
      }
    }

  }


  /**
   * @description: Método encargado de validar que el texto ingresado corresponda a las validaciones
   *               establecidas en el controlador de grupo (contactForm). En caso de ser inválido, 
   *               asignamos un mensaje de error.
   */
  public checkLastName() {

    if ( this.lastName.status === 'INVALID' ) {
      if ( this.lastName.value === '' ) {
        this.errorMessageLastName = 'Campo requerido';
      } else {
        this.errorMessageLastName = 'Valor incorrecto';
      }
    }

  }

  
  /**
   * @description: Método encargado de validar que el texto ingresado corresponda a las validaciones
   *               establecidas en el controlador de grupo (contactForm). En caso de ser inválido, 
   *               asignamos un mensaje de error.
   */
  public checkPhone() {

    if ( this.phone.status === 'INVALID' ) {
      if ( this.phone.value === '' ) {
        this.errorMessagePhone = 'Campo requerido';
      } else {
        this.errorMessagePhone = 'Valor incorrecto';
      }
    }

  }

  
  /**
   * @description: Método encargado de validar que el texto ingresado corresponda a las validaciones
   *               establecidas en el controlador de grupo (contactForm). En caso de ser inválido, 
   *               asignamos un mensaje de error.
   */
  public checkEmail() {

    if ( this.email.status === 'INVALID' ) {
      if ( this.email.value === '' ) {
        this.errorMessageEmail = 'Campo requerido';
      } else {
        this.errorMessageEmail = 'Valor incorrecto';
      }
    }

  }

  
  /**
   * @description: Método encargado de validar que el texto ingresado corresponda a las validaciones
   *               establecidas en el controlador de grupo (contactForm). En caso de ser inválido, 
   *               asignamos un mensaje de error.
   */
  public checkMessage() {

    if ( this.message.status === 'INVALID' ) {
      if ( this.message.value === '' ) {
        this.errorMessage = 'Campo requerido';
      } else {
        this.errorMessage = 'Valor incorrecto';
      }
    }
  }

}
