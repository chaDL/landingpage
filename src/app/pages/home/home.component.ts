import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  /* Image Icon */
  public iconFb = '../../../assets/icons/facebook_white.png';
  public iconIG = '../../../assets/icons/instagram_white.png';
  public iconYT = '../../../assets/icons/youtube_white.png';

  constructor() { }

  ngOnInit(): void {
  }

}
