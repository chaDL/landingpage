import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quienes-somos',
  templateUrl: './quienes-somos.component.html',
  styleUrls: ['./quienes-somos.component.scss']
})
export class QuienesSomosComponent implements OnInit {

  /* Rutas de Imágenes */
  private imgOne = "../../../assets/images/flower_blue.jpg";
  private imgTwo = "../../../assets/images/flower_pink.jpg";
  private imgThree = "../../../assets/images/flower.jpg";
  private imgFour = "../../../assets/images/perrito.jpg";

  public next = "../../../assets/icons/next.png";
  public back = "../../../assets/icons/back.png";

  public imageView: any;

  /* Count */
  public position = 0;

  /* Flags */
  public nextBtn = true;
  public backBtn = false;

  constructor() {
    this.imageView = this.imgOne;
  }

  ngOnInit(): void {
  }

  public nextImage() {

    if ( this.position <= 3 ) {
      this.position += 1;

      switch(this.position) {
        case 1:
          this.imageView = this.imgTwo;
          this.backBtn = true;
          break

        case 2: 
          this.imageView = this.imgThree;
          break;

        case 3: 
          this.imageView = this.imgFour;
          this.nextBtn = false;
          break;

        default:
          break;
      }
    }

  }


  public backImage() {

    if ( this.position >= 0 ) {
      this.position -= 1;

      switch( this.position ) {

        case 0:
          this.imageView = this.imgOne;
          this.backBtn = false;
          this.nextBtn = true;
          break;

        case 1:
          this.imageView = this.imgTwo;
          this.backBtn = true;
          break;

        case 2:
          this.imageView = this.imgThree;
          break;

        default:
          break;
      }

    }

  }

}
