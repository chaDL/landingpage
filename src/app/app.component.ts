import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  private innerWidth: any;

  public isMobile = false;

  constructor() {

    //
    this.innerWidth = window.innerWidth;

    this.getSizeScreen();
  }

  private getSizeScreen() {

    if ( this.innerWidth <= 1000 ) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }

  }

}
