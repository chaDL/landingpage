import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

/* Importaciones de rutas */
import { AppRoutingModule } from './app-routing.module';

/* Importaciones de Angular Material */
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';

/* Importaciones de Form */
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* Importaciones de Componentes */
import { AppComponent } from './app.component';
import { ObjetivoComponent } from './pages/objetivo/objetivo.component';
import { HomeComponent } from './pages/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { QuienesSomosComponent } from './pages/quienes-somos/quienes-somos.component';
import { VideoComponent } from './pages/video/video.component';
import { ContactoComponent } from './pages/contacto/contacto.component';
import { FooterComponent } from './pages/footer/footer.component';
import { MenuComponent } from './components/menu/menu.component';

@NgModule({
  declarations: [
    AppComponent,
    ObjetivoComponent,
    HomeComponent,
    QuienesSomosComponent,
    VideoComponent,
    ContactoComponent,
    FooterComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,

    FormsModule,
    ReactiveFormsModule,

    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
